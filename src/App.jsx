import { useState } from 'react';
import Button from './components/Button/Button';
import ModalBase from './components/Modal/ModalBase';
import ModalImage from './components/Modal/ModalImage';
import './App.scss';



const App = (props) =>{

  const [isModal, setIsModal] = useState(false);
  const [isModalImage, setIsModalImage] = useState(false);
  const handleModal = () => setIsModal(!isModal);
  const handleModalImage = () => setIsModalImage(!isModalImage);

  return (
   <> 
    <div className='container'>
      <Button boxView click={handleModal}>Open first modal</Button>
      <Button boxView click={handleModalImage}>Open second modal</Button>

      <ModalBase isOpen={isModal} handleClose={handleModal} />
      <ModalImage isOpen={isModalImage} handleClose={handleModalImage} />

    </div>
    </>
  );

}


export default App;
