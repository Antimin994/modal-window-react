import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import "./Modal.scss"


const ModalBase = ({handleOk, handleClose, isOpen}) =>{

    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
            handleClose()
        }
    }


    return(
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                   <ModalClose click={handleClose}/>
                </ModalHeader>
                <ModalBody>
                    <img className='modal-image'></img>
                    <h4>Product Delete!</h4>
                    <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                </ModalBody>
                <ModalFooter textFirst="no, cancel" textSecondary="yes, delete" clickFirst={handleOk} clickSecondary={handleClose}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    desk: PropTypes.string,
    handleOk: PropTypes.string,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}



export default ModalBase
